﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppearSE : MonoBehaviour
{
    private AudioSource audioSource;
    // 鳴らす音声クリップ
    [SerializeField]
    private AudioClip appearSE;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(appearSE);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
