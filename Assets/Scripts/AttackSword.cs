﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSword : MonoBehaviour
{
    private MyStatus myStatus;

    private void Start()
    {
        myStatus = transform.root.GetComponent<MyStatus>();
    }

    void OnTriggerEnter(Collider col)
    {
        
        if (col.tag == "Enemy")
        {
           //col.GetComponent<MoveEnemy>().TakeDamage(col.ClosestPointOnBounds(transform.position));
             var enemyScript = col.GetComponent<MoveEnemy>();
             if (enemyScript.GetState() != MoveEnemy.EnemyState.Damage && enemyScript.GetState() != MoveEnemy.EnemyState.Dead)
             {
                col.GetComponent<MoveEnemy>().TakeDamage(myStatus.GetAttackPower(), col.ClosestPointOnBounds(transform.position));
                GetComponent<ParticleSystem>().Play();
             }
            

        }
        else if (col.tag == "BigEnemy")
        {
           //col.GetComponent<MoveEnemy>().TakeDamage(col.ClosestPointOnBounds(transform.position));
            var enemyScript = col.GetComponent<MoveEnemy>();
             if (enemyScript.GetState() != MoveEnemy.EnemyState.Damage && enemyScript.GetState() != MoveEnemy.EnemyState.Dead)
             {
                col.GetComponent<MoveEnemy>().TakeDamage(myStatus.GetAttackPower(), col.ClosestPointOnBounds(transform.position));
                GetComponent<ParticleSystem>().Play();
             }
           
        }
    }
}
 

        