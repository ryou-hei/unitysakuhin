﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//CapsuleColliderとRigidbodyを追加
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]

public class DamageMotion : MonoBehaviour
{

    Animator animator;
    CapsuleCollider capsuleCollider;
    Rigidbody rb;

    void Start()
    {
        animator = GetComponent<Animator>();

        //CapsuleColliderコンポーネントを取得
        capsuleCollider = GetComponent<CapsuleCollider>();
        //CapsuleColliderの中心の位置を決める
        capsuleCollider.center = new Vector3(0, 0.76f, 0);
        //CapsuleColliderの半径を決める
        capsuleCollider.radius = 0.23f;
        //CapsuleColliderの高さを決める
        capsuleCollider.height = 1.6f;

        //Rigidbodyコンポーネントを取得
        rb = GetComponent<Rigidbody>();
        //RigidbodyのConstraintsを3つともチェック入れて
        //勝手に回転しないようにする
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        //勝手に移動しないようにする
        rb.constraints = RigidbodyConstraints.FreezePosition;
    }

    //IsTriggerにチェックが入れられた別のColliderに触れると
    //ダメージのアニメーションが出る
    void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.tag == "Sword"){
           animator.SetBool("Damage", true);
       }  
    }

    //アニメーションイベントによりDamageStateから
    //Idle1Stateに戻る
    void DamageEnd()
    {
        animator.SetBool("Damage", false);
    }
}