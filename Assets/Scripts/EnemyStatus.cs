﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStatus : MonoBehaviour
{

    // 敵の最大HP
    [SerializeField]
    private int maxHp ;
    
    
    //　敵のHP
    //[SerializeField]
    private int hp;
    
    private MoveEnemy enemy;

    // 敵の攻撃力
    public float power;

    // 敵の守備力
    [SerializeField]
    private int Defence;

    //　HP表示用UI    
    [SerializeField]
    private GameObject HPUI;

    //　HP表示用スライダー
    private Slider hpSlider;

    // 敵を倒した時の経験値
    private int EXP = 5;

    // 空の経験値
    private int exp = 0;


    GameObject Player;

    MyStatus mystatus;

    void Start() {
        exp = EXP;
        Player = GameObject.Find("Player");
        mystatus = Player.GetComponent<MyStatus>();
        enemy = GetComponent<MoveEnemy>();
        hp = maxHp;
        hpSlider = HPUI.transform.Find("HPbar").GetComponent<Slider>();      
        hpSlider.value = 1f;
       
    }

    public void SetHp(int hp)
    {
        this.hp = hp;
        
        //　HP表示用UIのアップデート
        UpdateHPValue();

            if (hp <= 0)
            {

            
            //　HP表示用UIを非表示にする
            HideStatusUI();
            }
        
    }

    public int GetHp()
    {
        return hp;
    }

  
    //　死んだらHPUIを非表示にする
    public void HideStatusUI()
    {
        HPUI.SetActive(false);
    }

    public int GetMaxHp() {
        return maxHp;
    }
    public int Getdefence()
    {
        return Defence;
    }
    
    public void UpdateHPValue() {
        hpSlider.value = (float)GetHp() / (float)GetMaxHp();
       // hpSlider.value = (float)GetHp() / hikakuHP;
    }


}
