﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    // HPの数値表示
    GameObject Chp;　
    //GameObject attackpower; //
    
    // Enemyオブジェクト
    GameObject enemy;
   // enemyDirector EnemyDirector;
    
   

    // enemyStatusオブジェクト
    EnemyStatus enemyStatus;

    // myStatusオブジェクト
    MyStatus myStatus;

    // 敵の攻撃力代入用
    float EnemyPower;

    // プレイヤーのHP代入用
    float PlayerHP;

    // プレイヤーの守備力代入用
    float PlayerDef;

    // 守備力
    float mamori = 30;
    

    float hp = 0.4f;
    // HPの下限
    float minhp = 0;
    // HPの上限
    float maxhp = 400;

    // HP上限(最新版)
    float MAXHP;

    // 現在のHP
    float currenthp;

    // HPバー
    public Slider slider;

    float CurrentHP;

    float Hp = 0f;

    // Start is called before the first frame update
    void Start()
    {

        // エネミーを取得
        enemy = GameObject.Find("Enemy");

        // EnemyStatusを取得
        enemyStatus = enemy.GetComponent<EnemyStatus>();

        // MyStatusを取得
        myStatus = GetComponent<MyStatus>();

        //EnemyDirector = enemy.GetComponent<enemyDirector>();  
        // HPゲージ
        slider.value = 1;
        // 現在のHPに代入
        currenthp = maxhp;
        
        // デバッグログで確認
        //Debug.Log("currenthp : " + currenthp);
        // HPのゲームオブジェクトを取得
        this.Chp = GameObject.Find("HP");
        // 敵の攻撃を取得
        //  this.attackpower = GameObject.Find("AttackPower");
        this.Chp.GetComponent<Text>().text = "HP " + currenthp;

        // 敵の攻撃力(上のとは別)
        EnemyPower = enemyStatus.power;
        // プレイヤーHP
        currenthp = myStatus.HP;
        //PlayerHP = myStatus.HP;
        PlayerDef = myStatus.Def;

        MAXHP = myStatus.MaxHP;

       // Debug.Log("EnemyPower : " + EnemyPower);
       // Debug.Log("PlayerHP : " + currenthp);
       // Debug.Log("PlayerDef : " + PlayerDef);
       // Debug.Log("上限HP : " + MAXHP);

    }
  
    public void OnTriggerEnter(Collider collider){
        if(collider.gameObject.tag == "EnemyAttack"){
           // float DethHP = 0;

           // 敵の攻撃
          // float damage = 200;
            float HABA = Random.Range(1, 4);

            // 最新バージョン
            // 更新後のHP = 現在のHP - 敵の攻撃 
             currenthp = currenthp - (((int)EnemyPower / 2) - ((int)PlayerDef / 2) -(int) HABA);
           　 //Debug.Log("After currentHP : " + currenthp);
            // 従来バージョン
            // currenthp = currenthp - ((damage/4)- (mamori / 2)- HABA);
            //Debug.Log("After currenthp : " + currenthp);

            if (currenthp < Hp)
            {
                this.Chp.GetComponent<Text>().text = "HP " + currenthp;
                currenthp = Hp;
                slider.value = currenthp / maxhp; ;
                Debug.Log("After currentHP : " + currenthp);
            }


            float overminhp = 1;
       　　 float hikakuHP = 0;
            
           
            // HPを代入
           hikakuHP = currenthp; 
           if( hikakuHP < overminhp ){
              //currenthp = overminhp;
                currenthp = hikakuHP;

                //slider.value = currenthp / maxhp; ;
                this.Chp.GetComponent<Text>().text = "HP " + currenthp;
                SceneManager.LoadScene("GameOverScene");
            }
          
           else {
           this.Chp.GetComponent<Text>().text = "HP " + currenthp;
           slider.value = currenthp / maxhp; ;
           //slider.value = (float)currenthp / (float)maxhp; ;
           //Debug.Log("slider.value : " +slider.value);
           }
        }   
           
        if(collider.gameObject.tag == "HPPoint"){

          float overmaxhp = MAXHP;
           // float overmaxhp = 400; // 上限HP
          float hikakuhp = 0;   // 
          float Point = maxhp * hp ; // 最大HPの40%を回復
          hikakuhp = currenthp + Point; // 現在のHP+最大HP40%分
          this.Chp.GetComponent<Text>().text = "HP " + currenthp;
          
          if( hikakuhp > overmaxhp){
            currenthp = overmaxhp;
            // HPの値
            this.Chp.GetComponent<Text>().text = "HP " + currenthp;
            //this.GetComponent<Text>().text = "HP" +  ;
           //currenthp = System.Math.maxhp(currenthp,minhp);
           //currenthp = System.Math.minhp(currenthp,maxhp);
          } 
          else{
            currenthp = currenthp + Point;
            // HPの値
            this.Chp.GetComponent<Text>().text = "HP " + currenthp; 
          }
           
           slider.value = (float)currenthp / (float)maxhp; ;
           // 回復後のHP
          　//Debug.Log("After currenthp : " + currenthp);
           // 回復アイテム
          // GameObject obj = GameObject.Find ("HPPoint");
          // Destroy(obj);
          // this.Chp.GetComponent<Text>().text = "HP " + currenthp;
        }
        
    }
    void OnCollisionEnter(Collision other){
          if(other.gameObject.tag == "HPPoint"){
             Destroy(other.gameObject);
             this.Chp.GetComponent<Text>().text = "HP " + currenthp;
          }
    }

   
    
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
