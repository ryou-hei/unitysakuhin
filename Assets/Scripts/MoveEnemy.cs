﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveEnemy : MonoBehaviour
{
    public enum EnemyState
    {
        Walk,
        Wait,
        Chase,
        Attack,
        Freeze,
        Damage,
        Dead
    };
    
    // 代入用
    float CurrentHP;
    // 現在のHP
    float currentHP;
    // 最大値
    float maxHP = 500f;
    // HP表示用UI
    private GameObject HPUI;
    // HP表示用スライダー
    //public Slider hpSlider; 

    // 敵のステータス管理スクリプト
    private EnemyStatus enemyStatus;

    // プレイヤーのステータス管理スクリプト
    private MyStatus myStatus;

    private CharacterController enemyController;
    private Animator animator;
    //　目的地
    private Vector3 destination;
    //　歩くスピード
    [SerializeField]
    private float walkSpeed = 1.0f;
    //　速度
    private Vector3 velocity;
    //　移動方向
    private Vector3 direction;
    //　到着フラグ
    private bool arrived;
    //　SetPositionスクリプト
    private SetPosition setPosition;
    //　待ち時間
    [SerializeField]
    private float waitTime = 5f;
    //　経過時間
    private float elapsedTime;
    // 敵の状態
    private EnemyState state;
    //　プレイヤーTransform
    private Transform playerTransform;
    
    private float  hikakuHP = 500;
    // マイステータススクリプトを取得
     MyStatus mystatus;

    // プレイヤーオブジェクトを取得
    GameObject Player;

    float time;

    // エフェクトオブジェクトを取得
    private GameObject AttackEffects;

    //　攻撃をする時の音
    [SerializeField]
    private AudioClip attackSound;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {

        time = 0.0f;
        Player = GameObject.Find("Player");
        mystatus = Player.GetComponent<MyStatus>();

        //currentHP = maxHP;
        enemyStatus = GetComponent<EnemyStatus>();
       // hpSlider.value = 500;
        
        enemyController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        setPosition = GetComponent<SetPosition>();
        setPosition.CreateRandomPosition();
        velocity = Vector3.zero;
        arrived = false;
        elapsedTime = 0f;
        SetState(EnemyState.Walk);

        audioSource = GetComponent<AudioSource>();

    }

    //　攻撃した後のフリーズ時間
    [SerializeField]
    private float freezeTime = 0.5f;

    void Update()
    {
        //　見回りまたはキャラクターを追いかける状態
        if (state == EnemyState.Walk || state == EnemyState.Chase)
        {
            //　キャラクターを追いかける状態であればキャラクターの目的地を再設定
            if (state == EnemyState.Chase)
            {
                setPosition.SetDestination(playerTransform.position);
            }
            if (enemyController.isGrounded)
            {
                velocity = Vector3.zero;
                animator.SetFloat("Speed", 2.0f);
                direction = (setPosition.GetDestination() - transform.position).normalized;
                transform.LookAt(new Vector3(setPosition.GetDestination().x, transform.position.y, setPosition.GetDestination().z));
                velocity = direction * walkSpeed;
            }

            if (state == EnemyState.Walk)
            {
                //　目的地に到着したかどうかの判定
                if (Vector3.Distance(transform.position, setPosition.GetDestination()) < 0.7f)
                {
                    SetState(EnemyState.Wait);
                    animator.SetFloat("Speed", 0.0f);
                }
            }
            else if (state == EnemyState.Chase)
            {
                //　攻撃する距離だったら攻撃
                if (Vector3.Distance(transform.position, setPosition.GetDestination()) < 3f)
                {
                    SetState(EnemyState.Attack);
                }
            }
            //　到着していたら一定時間待つ
        }
        else if (state == EnemyState.Wait)
        {
            elapsedTime += Time.deltaTime;

            //　待ち時間を越えたら次の目的地を設定
            if (elapsedTime > waitTime)
            {
                SetState(EnemyState.Walk);
            }
            //　攻撃後のフリーズ状態
        }
        else if (state == EnemyState.Freeze)
        {
            elapsedTime += Time.deltaTime;

            if (elapsedTime > freezeTime)
            {
                SetState(EnemyState.Walk);
            }
        }
        velocity.y += Physics.gravity.y * Time.deltaTime;
        enemyController.Move(velocity * Time.deltaTime);
    }
    float seconds;

    //　敵キャラクターの状態変更メソッド
    public void SetState(EnemyState tempState, Transform targetObj = null)
    {
        state = tempState;
        if (tempState == EnemyState.Walk)
        {
            arrived = false;
            elapsedTime = 0f;
            setPosition.CreateRandomPosition();
        }
        else if (tempState == EnemyState.Chase)
        {
            //　待機状態から追いかける場合もあるのでOff
            arrived = false;
            //　追いかける対象をセット
            playerTransform = targetObj;
        }
        else if (tempState == EnemyState.Wait)
        {
            elapsedTime = 0f;
            arrived = true;
            velocity = Vector3.zero;
            animator.SetFloat("Speed", 0f);
        }
        else if (tempState == EnemyState.Attack)
        {
            velocity = Vector3.zero;
            animator.SetFloat("Speed", 0f);
            animator.SetBool("Attack", true);
            audioSource.PlayOneShot(attackSound);
        }
        else if (tempState == EnemyState.Freeze)
        {
            elapsedTime = 0f;
            velocity = Vector3.zero;
            animator.SetFloat("Speed", 0f);
            animator.SetBool("Attack", false);
        }
       
        else if (tempState == EnemyState.Damage)
        {
            velocity = Vector3.zero;
            animator.ResetTrigger("Attack");
            animator.SetTrigger("Damage");
           // Destroy(this.gameObject, 2.2f);
        }
        else if (tempState == EnemyState.Dead)
        {
            animator.SetTrigger("Dead");
            //Destroy(this.gameObject, 3f);
            velocity = Vector3.zero;
            Destroy(this.gameObject, 2.2f);
        }
    }
    //　敵キャラクターの状態取得メソッド
    public EnemyState GetState()
    {
        return state;
    }
    //　攻撃を受けた時のエフェクト
    [SerializeField]
    private GameObject damageEffect;

    //　手のコライダ
    [SerializeField]
    private SphereCollider handCollider;

   // public void TakeDamage(int damage, Vector3 attackedPlace)
   // {
      public void TakeDamage(int damage, Vector3 attackedPlace)
        {
       // ダメージの幅
       int haba = Random.Range(1, 4);
        SetState(EnemyState.Damage);
        handCollider.enabled = false;
       // var damageEffectIns = Instantiate<GameObject>(damageEffect);
       // damageEffectIns.transform.position = attackedPlace;
        //Destroy(damageEffectIns, 1f);
        // ダメージ計算式
        enemyStatus.SetHp((enemyStatus.GetHp())- ((damage/2)-(enemyStatus.Getdefence()/4)- haba)  );
        // ダメージを受けた後のHP
        CurrentHP = enemyStatus.GetHp();
        
        //Debug.Log("敵のHP"+CurrentHP);
        time += Time.deltaTime;

        //var damageEffectIns = Instantiate<GameObject>(damageEffect);
       // damageEffectIns.transform.position = attackedPlace;
        //Destroy(damageEffectIns, 1f);

        if (enemyStatus.GetHp() <= 0)
        {
            Dead();
            if(gameObject.tag == "BigEnemy")
            {    
                    SceneManager.LoadScene("GameClearScene");          
            }                 
        }
    }
    void Dead()
    {
        SetState(EnemyState.Dead);
    }
}