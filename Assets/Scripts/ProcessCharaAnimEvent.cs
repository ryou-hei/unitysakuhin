﻿using System.Collections;
using System.Collections.Generic;
using UnityChan;
using UnityEngine;


public class ProcessCharaAnimEvent : MonoBehaviour
{



    private CharacterScript characterScript;
    [SerializeField]
    private CapsuleCollider capsuleCollider;

    private AudioSource audioSource;
    [SerializeField]
    private AudioClip attackSound;
    [SerializeField]
    private Transform equipTransform;

    private void Start()
    {
        characterScript = GetComponent<CharacterScript>();
    }

    void AttackStart()
    {

        capsuleCollider.enabled = true;
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(attackSound);
       

    }

    void AttackEnd()
    {
        capsuleCollider.enabled = false;
    }

    void StateEnd()
    {
        characterScript.SetState(CharacterScript.MyState.Normal);
    }

    public void EndDamage()
    {
        characterScript.SetState(CharacterScript.MyState.Normal);
    }

}
