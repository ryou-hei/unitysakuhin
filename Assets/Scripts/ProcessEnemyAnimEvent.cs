﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessEnemyAnimEvent : MonoBehaviour
{
    private MoveEnemy Enemy;
    [SerializeField]
    private SphereCollider sphereCollider;
 
    void Start() {
        Enemy = GetComponent<MoveEnemy>();
    }
 
    void AttackStart() {
        sphereCollider.enabled = true;
    }
 
    public void AttackEnd() {
        sphereCollider.enabled = false;
    }
 
    public void StateEnd() {
        Enemy.SetState(MoveEnemy.EnemyState.Freeze);
    }
 
    public void EndDamage() {
        Enemy.SetState(MoveEnemy.EnemyState.Walk);
    }
}
