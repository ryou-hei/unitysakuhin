﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcessEnemyAnimEvent1 : MonoBehaviour
{
     private Enemy enemy;
    [SerializeField]
    private SphereCollider sphereCollider;
 
    void Start() {
        enemy = GetComponent<Enemy>();
    }
 
    void AttackStart() {
        sphereCollider.enabled = true;
    }
 
    public void AttackEnd() {
        sphereCollider.enabled = false;
    }
 
    public void StateEnd() {
        enemy.SetState(Enemy.EnemyState.Freeze);
    }
 
    public void EndDamage() {
        enemy.SetState(Enemy.EnemyState.Walk);
    }
}
