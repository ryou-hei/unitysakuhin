﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Status : MonoBehaviour
{

    // レベル
    private int level = 1;       
    // HP
    private int hp = 20;
    // 最大HP
    private int maxhp = 20;
    // 攻撃力
    private int AttackPower = 5; 
     
    public int Level() {
        return level;
    }
    
    public int HP() {
        return hp;
    }
    
    public int MaxHP() {
        return maxhp;
    }
    
    public int Attack(){
        return AttackPower;
    }
}
