﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponStatus : MonoBehaviour
{

    public enum WeaponType
    {
        Sword
    }

    [SerializeField]
    private int attackPower = 20;

    public int GetAttackPower()
    {
        return attackPower;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
